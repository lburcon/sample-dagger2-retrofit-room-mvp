package pl.lburcon.tab

import android.app.Application
import pl.lburcon.tab.model.di.ApplicationComponent
import pl.lburcon.tab.model.di.ApplicationModule
import pl.lburcon.tab.model.di.DaggerApplicationComponent
import pl.lburcon.tab.model.di.RetrofitModule

/**
 * Created by lukasz.burcon on 10.05.2018.
 */

class MainApplication : Application() {

    /**
     * Dagger component
     */
    private val mComponent by lazy {
        DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .retrofitModule(RetrofitModule(ConnectionUtils.API_URL))
                .build()
    }

    /**
     * Returns Dagger's component
     */
    fun getApplicationComponent(): ApplicationComponent = mComponent

}