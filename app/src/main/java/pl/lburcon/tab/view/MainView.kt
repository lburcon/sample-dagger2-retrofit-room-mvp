package pl.lburcon.tab.view

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Parcelable
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.main_view.*
import org.jetbrains.anko.toast
import pl.lburcon.tab.GlideApp
import pl.lburcon.tab.MainApplication
import pl.lburcon.tab.R
import pl.lburcon.tab.model.MarvelComicItem
import pl.lburcon.tab.model.di.ApplicationComponent
import pl.lburcon.tab.presenter.MainPresenter
import javax.inject.Inject

class MainView : AppCompatActivity() {

    private val BUNDLE_RECYCLER_LAYOUT = "saved_recycler_state"
    private val connectivityManager by lazy {
        getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

    @Inject
    lateinit var mainPresenter: MainPresenter

    private lateinit var mComponent: ApplicationComponent


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_view)

        initialize()

        getComicData()
    }

    override fun onStop() {
        super.onStop()
        mainPresenter.cancelRetrofitAsyncTask()
    }

    private fun getComicData() {
        mainPresenter.getMarvelComicData(connectivityManager)
    }

    private fun initialize() {
        initializeDagger()
        initializeRecyclerView()
        mainPresenter.setupView(this)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        if (savedInstanceState != null) {
            val savedRecyclerLayoutState = savedInstanceState.getParcelable<Parcelable>(BUNDLE_RECYCLER_LAYOUT)
            recycler_view.layoutManager.onRestoreInstanceState(savedRecyclerLayoutState)
        }
    }

    public override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState!!.putParcelable(BUNDLE_RECYCLER_LAYOUT, recycler_view.layoutManager.onSaveInstanceState())
    }

    private fun initializeRecyclerView() {
        recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
        }
    }

    fun handleRecyclerViewData(comicList: List<MarvelComicItem>?) {
        setProgressBarVisbility(View.GONE)
        if (comicList != null) {
            if (!comicList.isEmpty()) {
                recycler_view.adapter = MarvelAdapter(comicList, GlideApp.with(this))
                refresh_button.visibility = View.GONE
                text_information.visibility = View.GONE
                return
            }
        }
        text_information.visibility = View.VISIBLE
        setRefreshButton()
    }

    private fun setRefreshButton() {
        if (refresh_button.visibility != View.VISIBLE) {
            refresh_button.visibility = View.VISIBLE
            refresh_button.setOnClickListener {
                getComicData()
                if (mainPresenter.isNetworkAvailable(connectivityManager)) {
                    text_information.visibility = View.GONE
                    refresh_button.visibility = View.GONE
                    setProgressBarVisbility(View.VISIBLE)
                } else {
                    toast(getString(R.string.toast_no_internet))
                }
            }
        }
    }

    private fun setProgressBarVisbility(visibility: Int) {
        progress_bar.visibility = visibility
        text_loading.visibility = visibility
    }

    private fun initializeDagger() {
        mComponent = (application as MainApplication).getApplicationComponent()
        mComponent.inject(this)
    }

}
