package pl.lburcon.tab.view

import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlinx.android.synthetic.main.item_marvel_recycler.view.*
import pl.lburcon.tab.ConnectionUtils
import pl.lburcon.tab.GlideRequests
import pl.lburcon.tab.R
import pl.lburcon.tab.model.MarvelComicItem

/**
 * Created by lukasz.burcon on 09.05.2018.
 */

class MarvelAdapter(private val data: List<MarvelComicItem>, private val glide: GlideRequests) :
        RecyclerView.Adapter<MarvelAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(comic: MarvelComicItem, glide: GlideRequests) = with(itemView) {
            textView.text = comic.title

            if (TextUtils.isEmpty(comic.imageUrl)) {
                comic.imageUrl = ConnectionUtils.prepareImageUrl(comic, System.currentTimeMillis())
            }

            glide
                    .asBitmap()
                    .load(comic.imageUrl)
                    .placeholder(R.drawable.not_available)
                    .dontTransform()
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_marvel_recycler, parent, false) as View
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position], glide)
    }

    override fun getItemCount() = data.size
}