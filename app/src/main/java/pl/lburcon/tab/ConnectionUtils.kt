package pl.lburcon.tab

import pl.lburcon.tab.model.MarvelComicItem
import java.security.NoSuchAlgorithmException


/**
 * Created by lukasz.burcon on 14.05.2018.
 */

class ConnectionUtils {
    companion object {
        val API_URL = "http://gateway.marvel.com"
        val API_KEY_PUBLIC = "059be8e24249fd3b9bb3b1235a20544b"
        private val API_KEY_PRIVATE = "e0a495d71711df28686f3bba9ae27174db604024"
        private val STANDARD_SMALL = "/landscape_large"
        private val API_KEY = "?ts="
        private val TIMESTAMP = "?apikey="
        private val HASH = "?hash="
        val MAP_PATH = "path"
        val MAP_EXTENSION = "extension"

        fun prepareHash(timestamp: String): String {
            return ConnectionUtils.md5Parse(timestamp + API_KEY_PRIVATE + API_KEY_PUBLIC)
        }

        fun prepareImageUrl(comic: MarvelComicItem, timestamp: Long): String {
            val stringBuilder = StringBuilder()
            stringBuilder.append(comic.thumbnail[MAP_PATH])
            stringBuilder.append(STANDARD_SMALL)
            stringBuilder.append(".")
            stringBuilder.append(comic.thumbnail[MAP_EXTENSION])
            stringBuilder.append(TIMESTAMP)
            stringBuilder.append(timestamp.toString())
            stringBuilder.append(API_KEY)
            stringBuilder.append(ConnectionUtils.API_KEY_PUBLIC)
            stringBuilder.append(HASH)
            stringBuilder.append(ConnectionUtils.prepareHash(timestamp.toString()))
            return stringBuilder.toString()
        }

        fun md5Parse(s: String): String {
            val MD5 = "MD5"
            try {
                // Create MD5 Hash
                val digest = java.security.MessageDigest
                        .getInstance(MD5)
                digest.update(s.toByteArray())
                val messageDigest = digest.digest()

                // Create Hex String
                val hexString = StringBuilder()
                for (aMessageDigest in messageDigest) {
                    var h = Integer.toHexString(0xFF and aMessageDigest.toInt())
                    while (h.length < 2)
                        h = "0" + h
                    hexString.append(h)
                }
                return hexString.toString()

            } catch (e: NoSuchAlgorithmException) {
                e.printStackTrace()
            }

            return ""
        }

    }
}
