package pl.lburcon.tab

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule


/**
 * Created by lukasz.burcon on 14.05.2018.
 */

@GlideModule
class MyAppGlideModule : AppGlideModule()