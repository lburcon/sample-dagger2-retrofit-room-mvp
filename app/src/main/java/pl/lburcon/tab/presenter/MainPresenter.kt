package pl.lburcon.tab.presenter

import android.net.ConnectivityManager
import pl.lburcon.tab.model.DataManager
import pl.lburcon.tab.model.MarvelComicItem
import pl.lburcon.tab.view.MainView

/**
 * Created by lukasz.burcon on 10.05.2018.
 */


class MainPresenter(private val dataManager: DataManager) : MainPresenterApi {
    private lateinit var mainView: MainView

    init {
        dataManager.setupPresenter(this)
    }

    override fun cancelRetrofitAsyncTask() {
        dataManager.cancelRetrofitAsyncTask()
    }

    override fun isNetworkAvailable(connectivityManager: ConnectivityManager): Boolean {
        return DataManager.isNetworkAvailable(connectivityManager)
    }


    override fun setupRecyclerView(comicList: List<MarvelComicItem>?) {
        mainView.handleRecyclerViewData(comicList)
    }

    override fun setupView(view: MainView) {
        mainView = view
    }

    override fun getMarvelComicData(connectivityManager: ConnectivityManager) {
        dataManager.getComicData(connectivityManager)
    }

}

interface MainPresenterApi {
    fun getMarvelComicData(connectivityManager: ConnectivityManager)
    fun setupView(view: MainView)
    fun setupRecyclerView(comicList: List<MarvelComicItem>?)
    fun isNetworkAvailable(connectivityManager: ConnectivityManager): Boolean
    fun cancelRetrofitAsyncTask()
}
