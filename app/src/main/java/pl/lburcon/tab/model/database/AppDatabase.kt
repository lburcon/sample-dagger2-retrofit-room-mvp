package pl.lburcon.tab.model.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

/**
 * Created by lukasz.burcon on 14.05.2018.
 */
@Database(entities = arrayOf(ComicModel::class), version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun comicDao(): ComicDao
}