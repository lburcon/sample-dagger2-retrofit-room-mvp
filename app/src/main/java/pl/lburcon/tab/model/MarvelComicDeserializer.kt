package pl.lburcon.tab.model

import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

/**
 * Created by lukasz.burcon on 13.05.2018.
 */
internal class MarvelComicDeserializer : JsonDeserializer<List<MarvelComicItem>> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): List<MarvelComicItem> {
        val data = json?.asJsonObject?.get("data")
        val result = data?.asJsonObject?.get("results")?.asJsonArray
        val listType = object : TypeToken<ArrayList<MarvelComicItem>>() {}.type

        return Gson().fromJson<List<MarvelComicItem>>(result, listType)
    }


}