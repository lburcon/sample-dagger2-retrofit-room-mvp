package pl.lburcon.tab.model.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

/**
 * Created by lukasz.burcon on 14.05.2018.
 */
@Dao
interface ComicDao {

    @Query("SELECT * FROM comic_data")
    fun getComicsData(): List<ComicModel>

    @Query("SELECT COUNT(*) FROM comic_data")
    fun getSize(): Int

    @Insert
    fun insertComicModel(comicModel: ComicModel)
}