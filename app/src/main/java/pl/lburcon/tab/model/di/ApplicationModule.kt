package pl.lburcon.tab.model.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by lukasz.burcon on 10.05.2018.
 */
@Module
class ApplicationModule(private val application: Application) {

    @Provides
    @Singleton
    fun provideApplicationContext(): Context = application.applicationContext


    @Provides
    @Singleton
    fun provideApplication(): Application = application
}