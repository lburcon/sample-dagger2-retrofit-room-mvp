package pl.lburcon.tab.model.di

import dagger.Component
import pl.lburcon.tab.view.MainView
import javax.inject.Singleton

/**
 * Created by lukasz.burcon on 10.05.2018.
 */
@Singleton
@Component(modules = arrayOf(ApplicationModule::class, RetrofitModule::class, MVPModelModule::class, DatabaseModule::class))
interface ApplicationComponent {
    fun inject(view: MainView)
}