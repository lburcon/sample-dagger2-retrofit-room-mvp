package pl.lburcon.tab.model.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by lukasz.burcon on 14.05.2018.
 */

@Entity(tableName = "comic_data")
data class ComicModel(
        @ColumnInfo(name = "title") var title: String = "",
        @ColumnInfo(name = "path") var path: String = "",
        @ColumnInfo(name = "extension") var extension: String = "",
        @ColumnInfo(name = "imageUrl") var imageUrl: String = ""
) {
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}