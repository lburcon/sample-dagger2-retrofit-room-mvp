package pl.lburcon.tab.model

import android.os.AsyncTask
import android.util.Log
import pl.lburcon.tab.ConnectionUtils
import pl.lburcon.tab.presenter.MainPresenter
import retrofit2.Response
import java.lang.ref.WeakReference

/**
 * Created by lukasz.burcon on 14.05.2018.
 */
class RetrofitAsyncTask(private val retrofitReference: WeakReference<RetrofitInterface>,
                        private val presenterReference: WeakReference<MainPresenter>,
                        private var dataManager: DatabaseApi?) : AsyncTask<Void, Void, List<MarvelComicItem>?>() {

    private val DEBUG_TAG = "DataManager"

    override fun doInBackground(vararg params: Void?): List<MarvelComicItem>? {
        val value: Response<List<MarvelComicItem>>?
        val timestamp = System.currentTimeMillis().toString()
        val hash = ConnectionUtils.prepareHash(timestamp)
        val retrofit = retrofitReference.get()

        if (!isCancelled && retrofit != null) {
            try {
                value = retrofit.getComics(timestamp, ConnectionUtils.API_KEY_PUBLIC, hash).execute()
                if (value != null) {
                    Log.i(DEBUG_TAG, value.toString())
                    Log.i(DEBUG_TAG, value.body().toString())
                } else {
                    return null
                }
            } catch (exception: Exception) {
                Log.e(DEBUG_TAG, exception.message)
                return null
            }

            val comicsList = value.body()
            if (comicsList != null) {
                prepareImageUrls(comicsList)
                if (!isCancelled) {
                    dataManager?.saveComicList(comicsList)
                    Log.d(DEBUG_TAG, "saved to database!")
                }
            }

            return comicsList
        }
        return null
    }

    private fun prepareImageUrls(comicsList: List<MarvelComicItem>) {
        comicsList.forEach {
            it.imageUrl = ConnectionUtils.prepareImageUrl(it, System.currentTimeMillis())
        }
    }

    override fun onPostExecute(result: List<MarvelComicItem>?) {
        presenterReference.get()?.setupRecyclerView(result)
    }

    override fun onCancelled() {
        super.onCancelled()
        Log.d(DEBUG_TAG, "AsyncTask got cancelled")
        dataManager = null
    }
}