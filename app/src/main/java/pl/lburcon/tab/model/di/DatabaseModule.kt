package pl.lburcon.tab.model.di

import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import pl.lburcon.tab.R
import pl.lburcon.tab.model.database.AppDatabase
import pl.lburcon.tab.model.database.ComicDao

/**
 * Created by lukasz.burcon on 14.05.2018.
 */

@Module(includes = arrayOf(ApplicationModule::class))
class DatabaseModule {

    @Provides
    fun provideApplicationDatabase(context: Context): AppDatabase =
            Room.databaseBuilder(context, AppDatabase::class.java, context.getString(R.string.db_name))
                    .fallbackToDestructiveMigration()
                    .build()

    @Provides
    fun provideComicDao(database: AppDatabase): ComicDao = database.comicDao()
}