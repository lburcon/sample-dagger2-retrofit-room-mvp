package pl.lburcon.tab.model

import android.net.ConnectivityManager
import android.util.Log
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import pl.lburcon.tab.ConnectionUtils.Companion.MAP_EXTENSION
import pl.lburcon.tab.ConnectionUtils.Companion.MAP_PATH
import pl.lburcon.tab.model.database.AppDatabase
import pl.lburcon.tab.model.database.ComicModel
import pl.lburcon.tab.presenter.MainPresenter
import java.lang.ref.WeakReference


/**
 * Created by lukasz.burcon on 10.05.2018.
 */

class DataManager(private val retrofit: RetrofitInterface, private val database: AppDatabase) : DataManagerApi, DatabaseApi {
    companion object {
        private val DEBUG_TAG = "DataManager"

        fun isNetworkAvailable(connectivityManager: ConnectivityManager): Boolean {
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }
    }

    private lateinit var mainPresenter: MainPresenter
    private var retrofitAsyncTask: RetrofitAsyncTask? = null

    override fun setupPresenter(presenter: MainPresenter) {
        mainPresenter = presenter
    }

    override fun isDatabaseEmpty(): Boolean {
        return database.comicDao().getSize() <= 0
    }

    override fun getComicDataFromDB(): List<MarvelComicItem> {
        val modelList = database.comicDao().getComicsData()
        val marvelComicsList = ArrayList<MarvelComicItem>()
        modelList.forEach {
            val map = HashMap<String, String>()
            map.put(MAP_PATH, it.path)
            map.put(MAP_EXTENSION, it.extension)
            marvelComicsList.add(MarvelComicItem(it.title, map, it.imageUrl))
        }
        Log.d("$DEBUG_TAG database", "comicData: $marvelComicsList")
        return marvelComicsList
    }

    override fun saveComicList(list: List<MarvelComicItem>) {
        list.forEach {
            val model = ComicModel(it.title, it.thumbnail[MAP_PATH]!!, it.thumbnail[MAP_EXTENSION]!!, it.imageUrl)
            database.comicDao().insertComicModel(model)
        }
    }

    override fun getComicData(connectivityManager: ConnectivityManager) {
        val dataManager = this
        doAsync {
            if (isNetworkAvailable(connectivityManager) && isDatabaseEmpty()) {
                Log.d(DEBUG_TAG, "Downloading data from server")
                retrofitAsyncTask = RetrofitAsyncTask(WeakReference(retrofit), WeakReference(mainPresenter), dataManager)
                retrofitAsyncTask?.execute()
            } else {
                Log.d(DEBUG_TAG, "Getting data from Database")
                val comicList = getComicDataFromDB()
                uiThread {
                    mainPresenter.setupRecyclerView(comicList)
                }
            }
        }
    }

    override fun cancelRetrofitAsyncTask() {
        if (retrofitAsyncTask != null && !retrofitAsyncTask!!.isCancelled) {
            retrofitAsyncTask?.cancel(true)
        }
    }


}

interface DataManagerApi {
    fun getComicData(connectivityManager: ConnectivityManager)
    fun getComicDataFromDB(): List<MarvelComicItem>
    fun isDatabaseEmpty(): Boolean
    fun setupPresenter(presenter: MainPresenter)
    fun cancelRetrofitAsyncTask()
}

interface DatabaseApi {
    fun saveComicList(list: List<MarvelComicItem>)
}

