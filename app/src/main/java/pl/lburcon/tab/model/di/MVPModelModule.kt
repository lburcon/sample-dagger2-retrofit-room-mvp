package pl.lburcon.tab.model.di

import dagger.Module
import dagger.Provides
import pl.lburcon.tab.model.DataManager
import pl.lburcon.tab.model.RetrofitInterface
import pl.lburcon.tab.model.database.AppDatabase
import pl.lburcon.tab.presenter.MainPresenter

/**
 * Created by lukasz.burcon on 10.05.2018.
 */

@Module(includes = arrayOf(RetrofitModule::class, DatabaseModule::class))
class MVPModelModule {

    @Provides
    fun provideDataManager(retrofit: RetrofitInterface, database: AppDatabase): DataManager = DataManager(retrofit, database)

    @Provides
    fun providePresenter(dataManager: DataManager) = MainPresenter(dataManager)
}