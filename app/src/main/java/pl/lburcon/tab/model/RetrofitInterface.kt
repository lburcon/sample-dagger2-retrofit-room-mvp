package pl.lburcon.tab.model

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by lukasz.burcon on 10.05.2018.
 */

interface RetrofitInterface {
    @GET("/v1/public/comics")
    fun getComics(@Query("ts") ts: String, @Query("apikey") apikey: String, @Query("hash") hash: String): Call<List<MarvelComicItem>>
}