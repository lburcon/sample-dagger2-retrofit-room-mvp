package pl.lburcon.tab.model

/**
 * Created by lukasz.burcon on 09.05.2018.
 */

data class MarvelComicItem(val title: String, val thumbnail: Map<String, String>,
                           var imageUrl: String)