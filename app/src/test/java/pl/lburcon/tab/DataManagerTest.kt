package pl.lburcon.tab

import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotSame
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.*
import pl.lburcon.tab.model.DataManagerApi
import pl.lburcon.tab.model.MarvelComicItem
import pl.lburcon.tab.presenter.MainPresenterApi

/**
 * Created by lukasz.burcon on 15.05.2018.
 */

class DataManagerTest {

    private lateinit var presenter: MainPresenterApi
    private lateinit var dataManager: DataManagerApi

    @Before
    fun setup() {
        presenter = mock(MainPresenterApi::class.java)
        dataManager = mock(DataManagerApi::class.java)

    }

    private fun <T> any(): T {
        Mockito.any<T>()
        return uninitialized()
    }
    @Suppress("UNCHECKED_CAST")
    private fun <T> uninitialized(): T = null as T

    @Test
    fun getDatabaseNonNullDataTest() {
        //Given
        val comic = prepareMarvelComic()
        val list = arrayListOf(comic, comic)
        val sizeZero = 0

        //When
        `when`(dataManager.getComicDataFromDB())
                .thenReturn(list)
        `when`(dataManager.isDatabaseEmpty()).thenReturn(false)

        //Then
        assertNotSame(dataManager.getComicDataFromDB().size, sizeZero)
        assertEquals(dataManager.isDatabaseEmpty(), false)
    }

    private fun prepareMarvelComic(): MarvelComicItem {
        val map = HashMap<String, String>()
        map.put("path", "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available")
        map.put("extension", "jpg")
        return MarvelComicItem("title", map, "")
    }

    @Test
    fun getDatabaseEmptyDataTest() {
        //Given
        val list = arrayListOf<MarvelComicItem>()
        val sizeZero = 0

        //When
        `when`(dataManager.getComicDataFromDB())
                .thenReturn(list)
        `when`(dataManager.isDatabaseEmpty()).thenReturn(true)

        //Then
        assertEquals(dataManager.getComicDataFromDB().size, sizeZero)
        assertEquals(dataManager.isDatabaseEmpty(), true)
    }

    @Test
    fun getComicDataTest() {
        //When
        doAnswer { presenter.setupRecyclerView(any()) }.`when`(dataManager).getComicData(any())

        dataManager.getComicData(any())

        //Then
        verify(presenter, times(1)).setupRecyclerView(any())
    }
}