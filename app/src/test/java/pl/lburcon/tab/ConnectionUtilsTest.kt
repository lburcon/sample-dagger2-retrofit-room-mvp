package pl.lburcon.tab

import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotSame
import org.junit.Test
import pl.lburcon.tab.model.MarvelComicItem


/**
 * Created by lukasz.burcon on 15.05.2018.
 */
class ConnectionUtilsTest {

    private fun prepareMarvelComic(): MarvelComicItem {
        val map = HashMap<String, String>()
        map.put("path", "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available")
        map.put("extension", "jpg")
        return MarvelComicItem("title", map, "")
    }

    @Test
    fun getImageUrlTest() {
        //Given
       val comic = prepareMarvelComic()

        //When
        val timestamp = System.currentTimeMillis()
        val hash = ConnectionUtils.prepareHash(timestamp.toString())
        val expectedImageUrl = "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available/landscape_large.jpg?ts=$timestamp?apikey=059be8e24249fd3b9bb3b1235a20544b?hash=$hash"
        val wrongImageUrlWithoutHash = "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available/landscape_large.jpg?ts=$timestamp?apikey=059be8e24249fd3b9bb3b1235a20544b"
        val wrongImageUrlWithoutApikey = "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available/landscape_large.jpg?ts=$timestamp?hash=$hash"
        val wrongImageUrlWithoutTimestamp = "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available/landscape_large.jpg?apikey=059be8e24249fd3b9bb3b1235a20544b?hash=$hash"
        val imageUrl = ConnectionUtils.prepareImageUrl(comic, timestamp)

        //Then
        assertEquals(expectedImageUrl, imageUrl)
        assertNotSame(expectedImageUrl, wrongImageUrlWithoutHash)
        assertNotSame(expectedImageUrl, wrongImageUrlWithoutApikey)
        assertNotSame(expectedImageUrl, wrongImageUrlWithoutTimestamp)
    }

    @Test
    fun getWrongImageUrlTest() {
        //Given
        val map = HashMap<String, String>()
        map.put("path", "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available")
        map.put("extension", "jpg")

        //When
        val timestamp = System.currentTimeMillis()
        val hash = ConnectionUtils.prepareHash(timestamp.toString())
        val expectedImageUrl = "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available/landscape_large.jpg?ts=$timestamp?apikey=059be8e24249fd3b9bb3b1235a20544b?hash=$hash"
        val wrongImageUrlWithoutHash = "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available/landscape_large.jpg?ts=$timestamp?apikey=059be8e24249fd3b9bb3b1235a20544b"
        val wrongImageUrlWithoutApikey = "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available/landscape_large.jpg?ts=$timestamp?hash=$hash"
        val wrongImageUrlWithoutTimestamp = "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available/landscape_large.jpg?apikey=059be8e24249fd3b9bb3b1235a20544b?hash=$hash"

        //Then
        assertNotSame(expectedImageUrl, wrongImageUrlWithoutHash)
        assertNotSame(expectedImageUrl, wrongImageUrlWithoutApikey)
        assertNotSame(expectedImageUrl, wrongImageUrlWithoutTimestamp)
    }

    @Test
    fun hashTest() {
        //Given
        val expectedHash = "e8cdfaa4a7a332bb5bd6cd2fc64f91f5"

        //When
        val timestamp = 12
        val hash = ConnectionUtils.prepareHash(timestamp.toString())
        //Then
        assertEquals(expectedHash, hash)
    }

    @Test
    fun wrongHashTest() {
        //Given
        val expectedHash = "e8cdfaa4a7a332bb5bd6cd2fc64f91f5"

        //When
        val wrongTimestamp = 13
        val wrongHash = ConnectionUtils.prepareHash(wrongTimestamp.toString())

        //Then
        assertNotSame(expectedHash, wrongHash)
    }

}