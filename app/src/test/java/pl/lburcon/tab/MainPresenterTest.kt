package pl.lburcon.tab

import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.*
import pl.lburcon.tab.model.DataManagerApi
import pl.lburcon.tab.presenter.MainPresenterApi

/**
 * Created by lukasz.burcon on 15.05.2018.
 */

class MainPresenterTest {

    private lateinit var presenter: MainPresenterApi
    private lateinit var dataManager: DataManagerApi

    @Before
    fun setup() {
        presenter = mock(MainPresenterApi::class.java)
        dataManager = mock(DataManagerApi::class.java)

    }

    private fun <T> any(): T {
        Mockito.any<T>()
        return uninitialized()
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T> uninitialized(): T = null as T

    @Test
    fun internetAvailableTest() {
        //When
        `when` (presenter.isNetworkAvailable(any()))
                .thenReturn(true)

        //Then
        assertEquals(presenter.isNetworkAvailable(any()), true)
    }

    @Test
    fun getComicDataTest() {
        //When
        doAnswer { presenter.setupRecyclerView(any()) }.`when`(presenter).getMarvelComicData(any())
        presenter.getMarvelComicData(any())

        //Then
        verify(presenter, times(1)).setupRecyclerView(any())
    }

    @Test
    fun cancelRetrofitAsyncTaskTest() {
        //When
        doAnswer { dataManager.cancelRetrofitAsyncTask() }.`when`(presenter).cancelRetrofitAsyncTask()
        presenter.cancelRetrofitAsyncTask()

        //Then
        verify(dataManager, times(1)).cancelRetrofitAsyncTask()
    }
}